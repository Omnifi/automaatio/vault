package command

import (
	"github.com/spf13/cobra"
)

// newCreateCommand creates the terminal "create" command for use with the command-line interface.
func newCreateCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:          "create",
		Short:        "Creates a Vault installation",
		Example:      `  vault install`,
		SilenceUsage: false,
	}

	command.Run = func(cmd *cobra.Command, args []string) {

	}

	return command
}
