module gitlab.com/omnifi/automaatio/vault/console

go 1.21.3

require (
	github.com/spf13/cobra v1.7.0
	gitlab.com/omnifi/automaatio/pulumi v0.0.0

)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)

replace gitlab.com/omnifi/automaatio/pulumi => gitlab.com/omnifi/automaatio/pulumi.git v0.0.0