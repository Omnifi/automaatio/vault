package main

import (
	command "gitlab.com/omnifi/automaatio/vault/console/command"
)

func main() {
	executeCommand()
}

// executeCommand constructs and executes the terminal / CLI commands.
func executeCommand() {
	rootCommand := command.NewRootCommand()
	rootCommand.Execute()
}
